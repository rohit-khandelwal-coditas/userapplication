package com.coditas.UserApplication.respository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.coditas.UserApplication.model.User;
import com.coditas.UserApplicationTest.UserApplication;

@Repository
public interface UserApplicationRespository extends JpaRepository<UserApplication, Integer>{

	List<User> exists(String emailId);

}
