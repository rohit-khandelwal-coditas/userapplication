package com.coditas.UserApplication.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.coditas.UserApplication.model.User;
import com.coditas.UserApplication.service.UserApplicationService;

@RestController
@RequestMapping("/api/admin")
public class UserApplicationController {

	@Autowired
	private UserApplicationService userApplicationService;
	List<User> lst = new ArrayList<User>();
	@GetMapping("/login")
	public List<User> userLogin(@PathVariable String emailId, @PathVariable String password) {
		return userApplicationService.login(emailId, password);
	}

	@PostMapping("/newuser")
	public User SignUp(@RequestBody User user) {
		return userApplicationService.createUser(user);
	}

	@PutMapping("/updateUser")
	public User updateUser(@RequestBody User user,@PathVariable String emailId) {
		return userApplicationService.updateUser(user,emailId);
	}

	@PutMapping("/changePassword")
	public void changepassword(@PathVariable String emailId,@PathVariable String password) {
		userApplicationService.changePassword(emailId,password);
	}
	
	@PutMapping("/forgotPassword")
	public void forgotpassword(@PathVariable String emailId) {
		userApplicationService.forgotPassword(emailId);
	}
}
