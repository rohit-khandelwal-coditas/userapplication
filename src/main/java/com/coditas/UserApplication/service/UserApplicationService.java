package com.coditas.UserApplication.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coditas.UserApplication.model.User;
import com.coditas.UserApplication.respository.UserApplicationRespository;

@Service
public class UserApplicationService {

	@Autowired
	private UserApplicationRespository userApplicationRespository;
	private User user;

	public User createUser(User user) {

		return userApplicationRespository.save(user);
	}

	public List<User> login(String emailId, String password) {
		return userApplicationRespository.exists(emailId);
	}

	public User updateUser(User user, String emailId) {
		user.setEmailId(emailId);
		return userApplicationRespository.save(user);
	}

	public void changePassword(String emailId, String password) {
		user.setEmailId(emailId);
		user.setPassword(password);
	}

	public void forgotPassword(String emailId) {
		user.setEmailId(emailId);

	}
}
